package com.anil.practices;
/**
 * @author anil maharjan 4/10/2019
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class PracticesApplication implements CommandLineRunner {

    @Autowired
    private CustomerRepository customerRepository;
    private final static String FILE_HEADER = "id, name, address, state";

    public static void main(String[] args) {
        SpringApplication.run(PracticesApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        CsvReader csvReader = new CsvReader();
        List<Customer> customerList = csvReader.readAndWriteCsv();
        System.out.println(customerList.toString());
        System.out.println("Reading and Saving in database");
        csvReader.readAndWriteCsv();
        //for copying
        customerList.forEach(customer -> customerRepository.save(customer));
        HashMap<String, List<Customer>> customerHashMap = new HashMap<>();
        List<Customer> customers = customerRepository.findAll();
        customers.forEach(customer -> {
            if (!customerHashMap.containsKey(customer.getState())) {
                ArrayList<Customer> customerArrayList = new ArrayList<>();
                customerArrayList.add(customer);
                customerHashMap.put(customer.getState(), customerArrayList);
            } else {
                List<Customer> customersByState = customerHashMap.get(customer.getState());
                customersByState.add(customer);
                customerHashMap.put(customer.getState(), customersByState);
            }
        });
        for (Map.Entry<String, List<Customer>> entry : customerHashMap.entrySet()) {
            String fileName = "state_" + entry.getKey() + ".csv";
            PrintWriter printWriter = new PrintWriter(fileName);
            printWriter.println(FILE_HEADER);
            List<Customer> customerLists = entry.getValue();
            customerLists.forEach(customer -> {
                printWriter.print(customer.getId());
                printWriter.print(",");
                printWriter.print(customer.getName());
                printWriter.print(",");
                printWriter.print(customer.getAddress());
                printWriter.print(",");
                printWriter.println(customer.getState());
            });
            printWriter.flush();
            printWriter.close();
        }
        System.out.println("Saved successfully");
    }
}
