package com.anil.practices;


import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class CsvReader {

    private final static String FILE_HEADER = "id, name, address, state";
    private final static String ELEMENT_SEPARATOR = ",";
    @Autowired
    CustomerRepository customerRepository;

    public List<Customer> readAndWriteCsv() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("customer.csv"));
        String line = bufferedReader.readLine();
        List<Customer> customerList = new ArrayList<>();
        int iteration = 0;
        while (line != null) {
            if (iteration == 0) {
                iteration++;
                line = bufferedReader.readLine();
                continue;
            }
            Customer customer = new Customer();
            String[] s = line.split(",");
            customer.setId(Long.parseLong(s[0]));
            customer.setName(s[1]);
            customer.setAddress(s[2]);
            customer.setState(s[3]);
            customerList.add(customer);
            line = bufferedReader.readLine();
        }
        PrintWriter printWriter = new PrintWriter("copy.csv");
        printWriter.println(FILE_HEADER);
        customerList.forEach(customer -> {
            printWriter.print(customer.getId());
            printWriter.print(ELEMENT_SEPARATOR);
            printWriter.print(customer.getName());
            printWriter.print(ELEMENT_SEPARATOR);
            printWriter.print(customer.getAddress());
            printWriter.print(ELEMENT_SEPARATOR);
            printWriter.print(customer.getState());
            printWriter.println();
        });
        printWriter.flush();
        printWriter.close();
        return customerList;
    }
}
